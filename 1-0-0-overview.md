## PART ONE

# Grokking Lisp

> "'Grok' means to understand so thoroughly that the observer becomes a part of the process being observed—to merge, to blend, to intermarry, to lose personal identity in group experience. It means almost everything that we mean by religion, philosophy, and science—and it means as little to us as color means to a blind man."
> <footer>Robert A. Heinlein, <em>Stranger in a Strange Land</em></footer>

Lisp isn't a language you just *learn*.  It changes you, as a programmer and a person.

In Part One, we focus on the core of the Common Lisp language, features that you will use every day, and techniques that will change the way you see programming.

We'll start with a high-level overview of the whole language, working through the details of syntax, semantics, style, and configuration of your development environment.

From there we'll move on to strings, characters, writing, and printing; prompting for user input; lists, trees, and special lists like alists and plists; math, vectors and arrays; lexical scope and variables; and functions.

Then we'll build our first application, a straight-forward text-adventure, using everything you've learned up to that point; extend this knowlege to a simple web application and command-line utilities, explore more advanced features of the language, and writing domain-specific languages for even more control over syntax.

## Chapters

1. [Common Lisp Bootcamp](1-01-00-lisp-bootcamp.html)
2. [Printing, Strings and Streams](1-02-00-input-output.html)
3. [Getting Input From Users](1-03-0-getting-input-from-users.html)
4. [Lists and List-Operations](1-04-0-lists.html)
5. [Look-up Lists and Trees](1-05-0-lookups-trees.html)
6. [Numbers and Math](1-06-0-math.html)
7. [Arrays and Vectors](1-07-0-arrays.html)
8. [Variables, Parameters, and Constants](1-08-0-variables.html)
9. [Closures](1-09-0-closures.html)
10. [Functions and Macros](1-10-0-functions.html)
11. [A Simple Text Adventure](1-11-0-text-adventure.html)
12. [Namespaces, Symbols, Packages, and Systems](1-12-0-namespaces.html)
13. [A Simple Web Application](1-13-0-simple-web-app.html)
14. [Conditionals](1-14-0-conditionals.html)
15. [Command-Line Utilities](1-15-0-command-line-utility.html)
16. [Mapping and Looping](1-16-0-map-loop.html)
17. [Revisiting Loops with Iterate](1-17-0-iterate.html)
18. [Format Strings](1-18-0-format.html)
19. [Domain Specific Languages](1-19-0-dsl.html)
20. [Part One in Review](1-20-0-review.html)
