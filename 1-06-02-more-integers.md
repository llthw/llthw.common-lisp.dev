# Exercise 1.6.2

## More Integers

Of course, this doesn't mean they're always the same type internally. Various integers have different types based on their size to conserve space, though they are all usable interchangeably.

### In the REPL

```lisp
(type-of 1)

(type-of 1234567890123456789012345678901234567890)

(type-of (expt -1234567890123456789012345678901234567890 147))

(typep 1 'integer)

(typep 1234567890123456789012345678901234567890 'integer)

(typep (expt -1234567890123456789012345678901234567890 147) 'integer)
```

### What You Should See

```lisp
* (type-of 1)
BIT

* (type-of 1234567890123456789012345678901234567890)
(INTEGER 4611686018427387904)

* (type-of (expt -1234567890123456789012345678901234567890 147))
BIGNUM

* (typep 1 'integer)
T

* (typep 1234567890123456789012345678901234567890 'integer)
T

* (typep (expt -1234567890123456789012345678901234567890 147) 'integer)
T
```
