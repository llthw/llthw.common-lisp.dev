# Exercise 1.4.8

## More Lists

Another way to create lists is using the `list` function.

```lisp
* (list 3 2 1)
(3 2 1)
```

The expression `(list a b ...)` is effectively shorthand for the expression `(cons a (cons b ...))`, with the final value being `cons`ed onto `NIL`.

```lisp
* (list 1 2 3)
(1 2 3)

* (cons 1 (cons 2 (cons 3 nil)))
(1 2 3)

* (equal (list 1 2 3) (cons 1 (cons 2 (cons 3 nil))))
T
```

As with `cons`, it's possible to build up trees, rather than merely lists, using `list`.

```lisp
* (list 1 (list 2 3) (list 4 (list (list 5) 6 7 8)))
(1 (2 3) (4 ((5) 6 7 8)))
```
