# Exercise 1.6.4

## Octal Integer Notation

### In the REPL

```lisp
#o77

#o00

#o01234567

(parse-integer "77" :radix 8)

(parse-integer "01234567" :radix 8)

(format nil "~o" 63)

(format nil "~o" 342391)

(type-of #o77)

(+ #o123 #o456)

(format nil "~o" #o01234567)
```

### What You Should See

There is also a built-in syntax for octal literals.

```lisp
* #o77
63

* #o0
0

* #o01234567
342391
```

There also exist appropriate supporting options in `parse-integer` and `format` to read or write octal numbers.

```lisp
* (parse-integer "77" :radix 8)
63
2

* (parse-integer "01234567" :radix 8)
342391
8

* (format nil "~o" 63)
"77"

* (format nil "~o" 342391)
"1234567"
```

And again, the `#o` reader form expands into a number...

```lisp
* (type-of #o77)
(INTEGER 0 4611686018427387903)
```

... which means you can use anywhere you could use numbers.

```lisp
* (+ #o123 #o456)
385

* (format nil "~o" #o01234567)
"1234567"
```
