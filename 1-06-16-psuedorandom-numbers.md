# Exercise 1.6.16

## Pseudo-Random Numbers

Pseudo-Random number generation is available with the `random` function; you
pass it a limit, which can either be a positive integer or float, and it returns
to you a number of the same type as the limit.

### In the REPL

```lisp
(random 10)

(random 10)

(random 10)

(random 10)

(random 10)

(random 10)
```

### What You Should See

```lisp
* (random 10)
3

* (random 10)
1

* (random 10)
5

* (random 10)
9

* (random 10)
6

* (random 10)
2
```
