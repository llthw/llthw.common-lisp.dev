# Exercise 1.6.15

## Trigonometry

### In the REPL

```lisp
pi

(sin pi)

(cos pi)

(tan pi)
```

### What You Should See

```lisp
* pi
3.141592653589793d0

* (sin pi)
1.2246467991473532d-16

* (cos pi)
-1.0d0

* (tan pi)
-1.2246467991473532d-16
```
