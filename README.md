# Learn Lisp The Hard Way

[![DOI](https://zenodo.org/badge/15644205.svg)](https://zenodo.org/badge/latestdoi/15644205)

```lisp
;; Welcome to L(λ)THW!
(let ((llthw '(learn lisp the hard way)))
  (format t "~{~@(~A~)~^ ~}, because ~{~A~^ ~} is easier!" llthw (cddr llthw)))

;; Common Lisp: Powerful, Expressive, Programmable, General Purpose, Multi-Paradigm.
```

This is the source-control repository for *Learn Lisp The Hard Way* (LLTHW), an
introduction to the Common Lisp programming language by "the Phoeron" Colin J.E.
Lupton, in the style of Zed Shaw's LxTHW series.

You can read the Second Draft (in-progress) of LLTHW online at:

https://llthw.common-lisp.dev

## Supporting This Project

If you would like to show your support for LLTHW, please consider sponsoring the
author through GitHub Sponsors:

https://github.com/sponsors/thephoeron

## Recent Updates

LLTHW is under active development and changes often. For a
list of recent updates, please see the [CHANGELOG](./CHANGELOG.md), and for a
summary of upcoming changes to look out for, please see the [TODO](./TODO.md)
list.

You can also check the commit history directly on the source-control repository
at:

https://gitlab.common-lisp.net/llthw/llthw.common-lisp.dev/-/commits/master

Updates to the LLTHW repository are automatically built and deployed to the live
server, typically within 5 minutes or less.

## Issues

To report technical problems with the content of this book, please create an
Issue on the source-control repository at:

https://gitlab.common-lisp.net/llthw/llthw.common-lisp.dev/-/issues

## Past Contributors

Past contributors from the Toronto Lisp User Group no longer involved in the
project include:

- Leo "Inaimathi" Zovic
- Dann Toliver
- Gaelen D'Costa
- Josh Teneycke

## LxTHW Repository

Zed Shaw released a "Learn X The Hard Way" open-source project so that anyone
could write an introductory book for their favourite programming language in the
style of the Learn Code The Hard Way series.

A fork of the original gitorious repo can be found at:

https://gitlab.common-lisp.net/llthw/learn-x-the-hard-way

You can learn more about the Learn Code The Hard Way series at:

https://learncodethehardway.org

## License

<span xmlns:dct="https://purl.org/dc/terms/" property="dct:title">Learn Lisp The Hard Way</span> by <a xmlns:cc="https://creativecommons.org/ns#" href="https://thephoeron.common-lisp.dev" property="cc:attributionName" rel="cc:attributionURL">"the Phoeron" Colin J.E. Lupton</a> is licensed under a <a rel="license" href="https://creativecommons.org/licenses/by-nd/4.0/">Creative Commons Attribution-NoDerivatives 4.0 International License</a>.

<center><a rel="license" href="https://creativecommons.org/licenses/by-nd/4.0/"><img alt="Creative Commons License" style="border-width:0;" src="https://i.creativecommons.org/l/by-nd/4.0/88x31.png" /></a></center>
