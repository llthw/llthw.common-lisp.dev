# PART THREE

## Lisp So(u)rcery

> "I would be disappointed if everything I saw turned out to be something Western Electric will build once Bell Labs works the bugs out. There ought to be some magic, somewhere, just for flavor."
> <footer>Robert A. Heinlein, <em>Glory Road</em></footer>

Now that you've gotten a solid grounding in the Common Lisp language and some popular Lisp libraries, it's time to really challenge yourself.  While up to this point, Extra Credit exercises have been mixed in-between fundamental language exercises, this part of the book is all practical exercises.  It will guide you through many of the popular use-cases you will come across as a professional Lisp developer, and by the end of it, you will be able to work some real programming magic.

This part of the book will also be less prescriptive, and leave you to rely on your knowledge and instinct to complete the outlined projects.  I will guide you through system architecture, laying out a project into user stories, and finding relevant libraries which can save you time; but I'm leaving the coding to you.  When you complete each chapter, you can then compare your implementation against my own reference implementation and those of your peers.  But remember, no cheating!  It's important that you finish the project on your own and get it to run with only the information I've given you, before looking for alternate implementations to improve your style or methodology.

### Chapters

1. [Real-World Web Apps](3-01-00-web-apps.md)
2. [Typesetting](3-02-00-typesetting.md)
3. [Native Mobile Applications](3-03-00-mobile.md)
4. [Cross-Platform Desktop Applications](3-04-00-gui.md)
5. [Drivers, Daemons, and System-Utilities](3-05-00-system-utils.md)
6. [Reverse Engineering](3-06-00-reverse-engineering.md)
7. [Graphics Rendering](3-07-00-graphics.md)
8. [OpenGL, SDL, and 3D Game Development](3-08-00-gaming.md)
9. [Audio Generation and Manipulation](3-09-00-audio.md)
10. [Data Aggregation and Analysis](3-10-00-data.md)
11. [Cryptography and Security](3-11-00-cryptosec.md)
12. [Financial Software and Crypto-Currencies](3-12-00-fintech.md)
13. [Scientific Computing](3-13-00-scientific-computing.md)
14. [Computational Physics](3-14-00-computational-physics.md)
15. [Quantum Computing](3-15-00-quantum-computing.md)
16. [Natural Language Processing](3-16-00-nlp.md)
17. [Artificial Intelligence](3-17-00-ai.md)
18. [Robotics](3-18-00-robotics.md)
19. [Space-Tech](3-19-00-space-tech.md)
20. [Neuroscience and Thought-Controlled Computing](3-20-00-neurotech.md)
21. [A Simple LispOS](3-21-00-lispos.md)
22. [Build Your Own Lisp Machine](3-22-00-lisp-machine.md)
23. [Government and Military Grade Systems](3-23-00-gov-mil.md)
