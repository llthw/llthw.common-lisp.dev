# Exercise 1.6.7

## Floating-Point Numbers

### In the REPL

```lisp
.1

1.5

1.1234

(typep 1.1234 'number)

(typep 1.1234 'integer)

(type-of 1.1234)

(/ 6.44444444444 2)

(+ .5000001 .4000009)
```

### What You Should See

Floating point numbers are also among the Common Lisp numeric types.

```lisp
* .1
0.1

* 1.5
1.5

* 1.1234
1.1234

* (typep 1.1234 'number)
T

* (typep 1.1234 'integer)
NIL

* (type-of 1.1234)
SINGLE-FLOAT
```

They suffer all the same precision issues here as in every other language you've seen them at work.

```lisp
* (/ 6.44444444444 2)
3.2222223

* (+ .5000001 .4000009)
0.90000105
```

So, as always, careful.
