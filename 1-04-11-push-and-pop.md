# Exercise 1.4.11

## PUSH and POP

The other half of the a stack involves destructively removing the first element from it. This can be done with `pop`.

```lisp
* *stack*
(3 2 1)

* (pop *stack*)
3

* *stack*
(2 1)

* (pop *stack*)
2

* (pop *stack*)
1

* *stack*
NIL
```

Calling `pop` on an empty list has no effect.

```lisp
* *stack*
NIL

* (pop *stack*)
NIL

* *stack*
NIL
```
