# Exercise 1.6.14

## Logarithms

### In the REPL

```lisp
(log 2)

(log 2 10)

(log 2 2)
```

### What You Should See

```lisp
* (log 2)
0.6931472

* (log 2 10)
0.30103

* (log 2 2)
1.0
```
