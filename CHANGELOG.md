# CHANGELOG

## 2022-09-05

- Moved Exercise 1.1.8, "Configuring Your Development Environment" to Preface, revised, and added a new section on working from the command-line
- Incremented Preface part numbers appropriately
- Reformatted Preface titles

## 2022-09-02

- Added full FAQ from old LLTHW web-app, revised as necessary to reflect migration to GitBook for GitLab Pages, and moved to more prominent separate document from title page

## 2022-09-01

- Normalized use of tense in the CHANGELOG
- Chapter 1.4, "Lists and List Operations"
    - Revised Exercise 1.4.2, restructuring into separate "In the REPL and What You Should See" subsections

## 2022-08-31

- Updated README.md for the LLTHW source-control repository

## 2022-08-30

- Chapter 1.4, "Lists and List Operations"
    - Revised Exercise 1.4.1, restructuring into separate "In the REPL" and "What You Should See" subsections the way LxTHW books are supposed to be
    - Split all exercises into separate documents and adjusted Markdown formatting accordingly
    - Renamed Exercise 1.4.10 from "Push and Pop" to "Stacks"
    - Renamed Exercise 1.4.11 from "Pop" to "PUSH and POP"
- Update TODO list
    - Reformatted as an unordered list because the version of GitBook for Gitlab Pages doesn't support Markdown checklists like the latest hosted GitBook platform does
    - Removed entry for Chapter 1.4
- Updated TOC to reflect structural changes to Chapter 1.4
- Added "About the Author" section to title page
- Updated "Recent Changes" section of title page to point readers to this CHANGELOG and the TODO list
- Reformatted list of past contributors to the LLTHW project as its own section of the title page

## 2022-08-29

- Exercise 1.1.8, "Configuring Your Development Environment"
    - Revised subsection on choice of text editors because my information was woefully out-of-date
    - Clarified subsection on being comfortable at the command-line as I think exactly how to word it

## 2022-08-26

- Fixed syntax highlighting for Exercise 1.5.12, "More Tries"
- Updated callout blocks to use "hints" plugin:
    - Exercise 1.2.3, "Unicode and Strings"
    - Exercise 1.2.5, "More Characters"
    - Exercise 1.2.6, "Character Codes"
    - Exercise 1.2.8, "Printing"
    - Exercise 1.2.11, "Printing with PRINC"
    - Exercise 1.2.16, "File Streams"
    - Chapter 1.11, "Extra Credit: A Simple Text Adventure"

## 2022-08-25

- Exercise 1.1.8, "Configuring Your Development Environment"
    - Cleaned-up inline hints
    - Removed deprecated section about the in-browser REPL
    - Clarified download and installation instructions for SBCL and Quicklisp
    - Rewrote and simplified section on choosing a text editor to avoid all talk
      of Emacs and Vim (like I should have done in the first place)

## 2022-08-24

- Added book.json to manually define GitBook configuration and fix some outstanding oddities
- Reorganized SUMMARY.md to generate a better TOC for "folding-chapters" plugin
- Added GitBook plugins:
    - "hints"
    - "folding-chapters"
    - "localized-footer"
    - "prism"

## 2022-08-23

- Added CHANGELOG and TODO chapters
- Deprecated and removed old "Draft Progress at a Glance" section from title page

## 2022-08-22

- Re-launched Learn Lisp The Hard Way on common-lisp.net
- Cleaned-up markdown source files for GitBook on GitLab Pages
- Removed purposely absurd "BigInt" examples from all exercises of Chapter 1.6,
  Numbers and Math, because GitBook couldn't parse them without crashing
- Removed Extra Credit Exercise 1.1.9, Setting Up and Learning Emacs Live
