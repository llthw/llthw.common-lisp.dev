# Exercise 1.4.10

## Stacks

We mentioned before that `cons`, `car`, `cdr` and friends are purely functional. But, sometimes, you want to destructively modify a list you've defined. For instance, in order to implement a mutable stack. In order to destructively `cons` elements onto a list, use `push`.

```lisp
* (defvar *stack* nil)
*stack*

* (push 1 *stack*)
(1)

* *stack*
(1)

* (push 2 *stack*)
(2 1)

* (push 3 *stack*)
(3 2 1)

* *stack*
(3 2 1)
```
