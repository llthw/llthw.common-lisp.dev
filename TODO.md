# TODO

- Chapter 1.3, "Extra Credit: Getting Input From Users"
    - Finish code examples
    - Split code examples into separate "In the REPL" and "What You Should See" sections
- Rewrite code examples and simplify discourse for Chapter 1.5, "Extra Credit: Look-up Lists and Trees"
- Write code examples for Chapter 1.7, "Extra Credit: Arrays and Vectors"
- Write code examples for Chapter 1.8, "Variables, Parameters, and Constants"
