# Exercise 1.4.1

## Cons-Cells

Cons-Cells are the smallest compound data structure in Lisp. A Cons-Cell is effectively a pair of pointers. You can tell if what you're looking at is a Cons-Cell by using the predicate `consp`, and you can create a Cons-Cell using the function `cons`.

### In the REPL

```lisp
(consp 5)

(consp "a")

(consp 'a)

(consp (cons 'a 'b))
```

### Wnat You Should See

Numbers, strings, and symbols are not Cons-Cells, so the `consp` predicate returns `nil`. You can create a Cons-Cell of two other objects with the `cons` function, which returns a fresh Cons-Cell every time it is called. This operation is called *consing*, and we will explore it further in the following exercises.

```lisp
* (consp 5)
NIL

* (consp "a")
NIL

* (consp 'a)
NIL

* (consp (cons 'a 'b))
T
```
