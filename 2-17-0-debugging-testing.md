# Chapter 2.17

## Debugging and Unit Testing

> "I know, as the prime lesson of my profession, that good intentions are the source of more folly than all other causes put together."
> <footer>Robert A. Heinlein, <em>Glory Road</em></footer>

No matter what you do, how hard you try, your code will eventually break.  You, or someone else maintaining your code, will make a mistake, or overlook that one use-case that brings your whole program down.  It happens.  Software is fragile by nature, and programmers *are* only human.  So remember this: the debugger is your friend.  It's only there to help you.

You've seen the interactive debugger enough times by now to have a general idea how it works, but Lisp has a lot more tools to help you debug as you go; you've already seen how to write and handle Conditions in Chapter 2.11---but that's just a small piece of a much bigger puzzle.

### Exercise 2.17.1

**The Debugger**

```lisp

```

### Exercise 2.17.2

**Control Stack**

```lisp

```

### Exercise 2.17.3

**Backtrace**

```lisp

```

### Exercise 2.17.4

**Restarts**

```lisp

```

### Exercise 2.17.5

**Stepping**

```lisp

```

### Exercise 2.17.6

**LDB, the low-level debugger**

```lisp

```

### Exercise 2.17.7

**More LDB**

```lisp

```

### Exercise 2.17.8

**Even More LDB**

```lisp

```

### Exercise 2.17.9

**Writing Tests**

```lisp

```

### Exercise 2.17.10

**Regression Testing with FiveAM**

```lisp

```

### Exercise 2.17.11

**More FiveAM**

```lisp

```

### Exercise 1.17.12

**Even More FiveAM**

```lisp

```

### Extra Credit Exercise 2.17.13

**Interactive Debugging in Emacs+SLIME**

```lisp

```
