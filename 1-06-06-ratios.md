# Exercise 1.6.6

## Ratios

### In the REPL

```lisp
3/17

1/2

(typep 1/2 'integer)

(typep 1/2 'number)

(type-of 1/2)

.5

(type-of .5)

1/2

4/8

32/64
```

### What You Should See

Ratios are a primitive numeric type in Common Lisp.

```lisp
* 3/17
3/17

* 1/2
1/2
```

Ratios are not integers, but they are still numbers.

```lisp
* (typep 1/2 'integer)
NIL

* (typep 1/2 'number)
T
```

These are not the same things as their float equivalents.

```lisp
* (type-of 1/2)
RATIO

* .5
0.5

* (type-of .5)
SINGLE-FLOAT
```

They are always reduced to lowest terms.

```lisp
* 1/2
1/2

* 4/8
1/2

* 32/64
1/2
```
