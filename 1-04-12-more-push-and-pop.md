# Exercise 1.4.12

## More PUSH and POP

Like `cons`, `push` isn't limited to the existing type of its target.

```lisp
* *stack*
NIL

* (push 1 *stack*)
(1)

* (push "b" *stack*)
("b" 1)

* (push 'c *stack*)
(c "b" 1)

* (push (list 4 5) *stack*)
((4 5) C "a" 1)

* *stack*
((4 5) C "a" 1)
```
