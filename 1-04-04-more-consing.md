# Exercise 1.4.4

## More Consing

Cons-Cells need not contain homogenous data.

```lisp
* (cons 'a 2)
(A . 2)

* (cons 1 "two")
(1 . "two")

* (cons "a" 'b)
("a" . B)
```
