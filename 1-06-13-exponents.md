# Exercise 1.6.13

## Exponents

### In the REPL

```lisp
(expt 2 2)

(expt 10 28)

(exp 28)
```

### What You Should See

```lisp
* (expt 2 2)
4

* (expt 10 28)
10000000000000000000000000000

* (exp 28)
1.4462571e12
```
