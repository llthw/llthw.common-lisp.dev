# Preface, Pt. VI

## Acknowledgements

> "Age is not an accomplishment, and youth is not a sin."
> <footer>Robert A. Heinlein, <em>Methuselah's Children</em></footer>

First, I would like to thank John McCarthy (1927--2011), the creator of LISP and father of modern AI.  His insights into axiomatizing computation led to the ultimate expression of the symmetry between the human mind and the universe through the art of programming.

To the generations of Lisp Hackers before me, for keeping the language alive even through the dark years of the AI Winter, and for their excellent contributions to open-source software.  And to all those who've lent a hand proofreading the first and second drafts of this work, for their time and feedback.

And lastly, to Zed A. Shaw for creating and releasing to open-source the LxTHW package for writing your own Learn Code The Hard Way book, so that I could finally bring my ideas into being for a better Lisp book; he has already gone a long way on his own to improve programming education as a whole, and to make programming more accessible to people all over the world.
