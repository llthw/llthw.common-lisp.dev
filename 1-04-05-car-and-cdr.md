# Exercise 1.4.5

## CAR and CDR

Using Cons-Cells as building blocks would be kind of pointless if we couldn't get their components back out. To get the value of the first slot in a Cons-Cell, we use the `car` function.

```lisp
* (cons 'a 'b)
(A . B)

* (car (cons 'a 'b))
A
```

Similarly, we can get the value from the second slot in a Cons-Cell using `cdr`.

```lisp
* (cons 1 2)
(1 . 2)

* (cdr (cons 1 2))
2
```
