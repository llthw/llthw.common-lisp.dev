# Chapter 1.2

## Printing, Strings, and Streams

> "There is no such thing as luck; there is only adequate or inadequate preparation to cope with a statistical universe."
> <footer>Robert A. Heinlein, <em>Have Space Suit---Will Travel</em></footer>

Now that you've got an idea of what Lisp is all about and you have your development environment configured to your liking, we can get moving on some real, practical code.

At the end of the day, no matter what the program you're working on does, at some point you will have to give something back to the user.  The more interactive the program, the more data will be going back and forth between the user and your program.  And users love interactivity---they like to know just how long a set of tasks will take, they want to see updates to data in real-time, they want feedback.

Since development in Lisp is so inherently interactive, it makes sense to think about your programs this way too.  So we will be starting our exploration of Common Lisp with the core language features that underlie input and output: printing, strings, and streams.

### Exercises:

* [Strings](1-02-01-strings.html)
* [More Strings](1-02-02-more-strings.html)
* [Unicode and Strings](1-02-03-unicode.html)
* [Characters](1-02-04-chars.html)
* [More Characters](1-02-05-more-chars.html)
* [Character Codes](1-02-06-char-codes.html)
* [Strings from Chars](1-02-07-strings-from-chars.html)
* [Printing](1-02-08-printing.html)
* [More Printing](1-02-09-more-printing.html)
* [Printing With prin1](1-02-10-prin1.html)
* [Printing With princ](1-02-11-princ.html)
* [A Brief Introduction to Format](1-02-12-format.html)
* [A Little Bit More on Format](1-02-13-more-format.html)
* [Pathnames](1-02-14-pathnames.html)
* [Streams](1-02-15-streams.html)
* [File Streams](1-02-16-file-streams.html)
* [Binary Streams](1-02-17-binary-streams.html)
* [Prompting Users](1-02-18-prompting-users.html)
* [Pretty-Printing](1-02-19-pretty-printing.html)
