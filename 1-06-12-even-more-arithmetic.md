# Exercise 1.6.12

## Even More Arithmetic

### In the REPL

```lisp
(mod (+ 10 2) 2)

(mod (+ 55 20) 60)
```

### What You Should See

```lisp
* (mod (+ 10 2) 2)
0

* (mod (+ 55 20) 60)
15
```
