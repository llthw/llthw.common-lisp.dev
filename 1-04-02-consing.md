# Exercise 1.4.2

## Consing

As we saw in the previous exercise, Cons-Cells can be created with the `cons` function. This operation is called *consing*.

### In the REPL

Let's try consing a few pairs of objects together.

```lisp
(cons 'a 'b)

(cons 1 2)

(cons "one" "two")
```

### What You Should See

The `cons` function returns a fresh Cons-Cell every time it is called. Since a Cons-Cell is just a pair of pointers it can combine any two Lisp objects into a pair, no matter what their type is.

```lisp
* (cons 'a 'b)
(A . B)

* (cons 1 2)
(1 . 2)

* (cons "one" "two")
("one" . "two")
```
Notice the special syntax of the Cons-Cell returned at the REPL? This is called Dot-Notation and we'll learn more about it in the next exercise.
