# Exercise 1.2.2

## More Strings

In Lisp, strings can contain any character without special markup, except for two particular characters which must be escaped; normally, what you type inside the double-quotes is exactly what you get.  Line-breaks and all.

Sometimes though, you want to include double-quote characters inside your string.  Lisp has a way of doing that, using the escape-character, `\`, the Backslash.

As it turns out, the backslash will escape *any* character.  So if you want to print a literal backslash, it too has to be escaped.

```lisp
* "this string contains \"double-quotes\"."
* "and this string has an escaped backslash: \\."
```

### What You Should See

```lisp
* "this string contains \"double-quotes\"."

"this string contains \"double-quotes\"."
* "and this string has an escaped backslash: \\."

"and this string has an escaped backslash: \\."
```

Once again, you get back exactly what you typed.
