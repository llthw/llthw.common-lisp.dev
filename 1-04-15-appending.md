# Exercise 1.4.15

## Appending

Putting lists together is the job of the `append` function.

```lisp
* (append (list 1 2 3) (list 4 5 6))
(1 2 3 4 5 6)

* (append (list 6 5 4 3) (list 2 1))
(6 5 4 3 2 1)
```

`append` is an example of a function that takes a `&rest` argument. Meaning you can pass it any number of lists...

```lisp
* (append (list 'a 'b 'c 'd) (list 'e 'f) (list 'g))
(A B C D E F G)

* (append (list 1) (list 2) (list 3) (list 4))
(1 2 3 4)
```

...though passing it one list is pointless.

```lisp
* (append (list 1 2 3))
(1 2 3)

* (list 1 2 3)
(1 2 3)
```
