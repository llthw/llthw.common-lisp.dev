# Exercise 1.6.5

## Binary Integer Notation

### In the REPL

```lisp
#b1

#b1011

#b101010101

(parse-integer "1011" :radix 2)

(parse-integer "101010101" :radix 2)

(format nil "~b" 11)

(format nil "~b" 341)

(type-of #b1)

(type-of #b101)

(+ #b10101 #b101)

(format nil "~b" #b101010101)
```

### What You Should See

Similarly, there is a binary literal notation.

```lisp
* #b1
1

* #b1011
11

* #b101010101
341
```

`format` and `parse-integer` work nicely to encode and decode binary numbers.

```lisp
* (parse-integer "1011" :radix 2)
11
4

* (parse-integer "101010101" :radix 2)
341
9

* (format nil "~b" 11)
"1011"

* (format nil "~b" 341)
"101010101"
```

And once again, the reader macro expands into a number.

```lisp
* (type-of #b1)
BIT

* (type-of #b101)
(INTEGER 0 4611686018427387903)
```
So all the usual number-related tricks are available.

```lisp
* (+ #b10101 #b101)
26

* (format nil "~b" #b101010101)
"101010101"
```
