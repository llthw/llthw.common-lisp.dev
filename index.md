---
page.title: Learn Lisp The Hard Way
author: \"the Phoeron\" Colin J.E. Lupton
---

<p align="center">
  <span style="font-size:36px;font-weight:bold;">Learn Lisp The Hard Way</span><br>
  <span style="font-size:32px;font-weight:bold;">(Second Draft, In-Progress)</span><br><br>
  <span style="font-size:18px;">by</span><br><br>
  <span style="font-size:32px;">"the Phoeron" Colin J.E. Lupton</span><br><br>
  </span>
</p>

```lisp
;; Welcome to L(λ)THW!
(let ((llthw '(learn lisp the hard way)))
  (format t "~{~@(~A~)~^ ~}, because ~{~A~^ ~} is easier!" llthw (cddr llthw)))

;; Common Lisp: Powerful, Expressive, Programmable, General Purpose, Multi-Paradigm.

```

{% hint style='danger' %}
This is a draft-in-progress edition of Learn Lisp the Hard Way. Content is being constantly added and revised. Please check back often for updates.
{% endhint %}

## About the Author

"the Phoeron" Colin J.E. Lupton is a full-time open-source developer (and
recovering serial entrepreneur). He first discovered the Common Lisp and Scheme
programming languages when he was 9 years old and has been in love with the
Lisp-family of languages ever since.

You can find him online on:

- common-lisp.net: [@thephoeron](https://gitlab.common-lisp.net/thephoeron)
- GitHub: [@thephoeron](https://github.com/thephoeron)
- Twitter: [@thephoeron](https://twitter.com/thephoeron)

## Supporting This Project

If you would like to show your support for this project, please consider
sponsoring the author through GitHub Sponsors:

https://github.com/sponsors/thephoeron

## Recent Updates

*Learn Lisp The Hard Way* is under active development and changes often. For a
list of recent updates, please see the [CHANGELOG](./CHANGELOG.md), and for a
summary of upcoming changes to look out for, please see the [TODO](./TODO.md)
list.

You can also check the commit history directly on the source-control repository
at:

https://gitlab.common-lisp.net/llthw/llthw.common-lisp.dev/-/commits/master

Updates to the LLTHW repository are automatically built and deployed to the live
server, typically within 5 minutes or less.

## Issues

To report technical problems with the content of this book, please create an
Issue on the source-control repository at:

https://gitlab.common-lisp.net/llthw/llthw.common-lisp.dev/-/issues

## Past Contributors

Past contributors from the Toronto Lisp User Group no longer involved in the
project include:

- Leo "Inaimathi" Zovic
- Dann Toliver
- Gaelen D'Costa
- Josh Teneycke
