# Exercise 1.6.10

## Arithmetic

Now that you've had a chance to explore some more number types built in to Common Lisp, let's take another look at what you can do with them.

### In the REPL

```lisp
(+ 1 1)

(1+ 1)

(- 1 1)

(- 1)

(* 2 2)

(/ 2 2)

(/ 10 2)

(/ 9 2)
```

### What You Should See

```lisp
* (+ 1 1)
2

* (1+ 1)
2

* (- 1 1)
0

* (1- 1)
0

* (- 1)
-1

* (* 2 2)
4

* (/ 2 2)
1

* (/ 10 2)
5

* (/ 9 2)
9/2
```

You can non-destructively increment or decrement a value with the built-in functions `1+` and `1-`, respectively.

You can negate a number by passing it as the only argument to the subtraction function.  This may seem verbose compared to the much simpler `-n` syntax for negative integers, however, this allows *any* number that can be negated to be, and also allows for user-supplied values to be negated.

Division normally returns a *rational* number, the canonical representation of which could be either an integer *or* a ratio.
