# Exercise 1.6.9

## Complex Numbers

Common Lisp has built-in support for Complex Numbers. You can perform arithmetic on them just like any other number type.

### In the REPL

```lisp
(sqrt -1)

(+ #C(0.0 1.0) #C(0.0 1.0))

(* #C(0.0 1.0) #C(0.0 1.0))
```

### What You Should See

```lisp
* (sqrt -1)
#C(0.0 1.0)

* (+ #C(0.0 1.0) #C(0.0 1.0))
#C(0.0 2.0)

* (* #C(0.0 1.0) #C(0.0 1.0))
#C(-1.0 0.0)
```
