# Exercise 1.2.7

## Strings From Characters

Most of the time, you'll want to work with strings instead of characters.  You can get a one-character string object directly from a character object by calling the `string` function on it:

```lisp
(string #\greek_small_letter_lamda)
```

But the `string` function only takes one argument, a character or a string, and isn't very smart.  It doesn't know how to do much in terms of type conversion.

It's also easy to get a string from a list of characters.  Remember the `coerce` function introduced in exercise 1.2.4? You can change the type argument from `'list` to `'string`, and then the `coerce` function knows you want convert the list into a sequence of type string:

```lisp
(coerce '(#\( #\greek_small_letter_lamda #\)) 'string)
```

### What You Should See

```lisp
* (string #\greek_small_letter_lamda)
"λ"
* (coerce '(#\( #\greek_small_letter_lamda #\)) 'string)
"(λ)"
```
