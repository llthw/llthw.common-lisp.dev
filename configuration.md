# Preface, Pt. IV

## Configuring Your Development Environment

Before diving in head-first to Common Lisp with [Chapter 1.1](./1-01-00-lisp-bootcamp.md), you'll want to spend a little time configuring your development environment so that you can focus on the code.  A lot of configuration choices are a matter of personal taste, and there are many excellent implementations of the Common Lisp language available, but for the sake of this book we will stick to a minimalist approach: working from the command-line with SBCL and a plain-text editor.

If you are an advanced user and already have a preferred development environment that supports Lisp, such as Emacs or vim, you can stick with it. The point is to feel comfortable, confident, and focused so that nothing interferes with your ability to learn Lisp.

### Working from the Command-Line

As a developer, it is important to be comfortable working at the command-line. For this book, we will focus on working with Lisp from the command-line, not advanced features of command-line shells themselves, so you can use whichever one you prefer or have installed by default.

If you've never used a command-line shell before, PowerShell 7 is open-source, cross-platform, and has a low barrier of entry, which means it's free, you'll be able to pick it up fast, and carry over your skills to any environment. It is available for Windows, Linux, and macOS.

Documentation on PowerShell 7, including download and installation instructions are available at:

https://microsoft.com/powershell

macOS, Linux, UNIX, and BSD typically provide Bash out-of-the-box and support other command-line shells such as ZSH.

Windows includes the old Windows PowerShell 5.1 and DOS command-line out-of-the-box, but it is recommended to upgrade to PowerShell 7 or use the Windows Subsystem for Linux (WSL) which includes Bash.

### Installing SBCL

Steel Bank Common Lisp, or SBCL for short, is one of the easiest Lisp implementations to get up and running&mdash;it's free and open-source, actively developed, is supported for a wide selection of platforms, and is considered to be the most robust open-source implementation available.  Since it automatically compiles everything you enter at the REPL, your running Lisp processes also benefit from a major speed-boost over other Lisps.

{% hint style='info' %}
The SBCL REPL is minimal by design, since it is primarily used from within Emacs+SLIME which provides its own custom prompt, history, completion, multi-line editing capabilities, and more.  In order to use it effectively straight from the command-line, you will need to install a separate tool on macOS and Linux called `rlwrap`, a readline wrapper. It provides evaluation history, multi-line editing, and can be configured for completions and filters.  You can read more about `rlwrap` here: https://github.com/hanslub42/rlwrap
{% endhint %}

You can learn more about SBCL at the official website: https://sbcl.org/

#### Windows

The latest SBCL installer packages for Windows, in separate versions for 32 and 64-bit systems, are available on the official downloads page.

Once installed, you can run SBCL from the Start menu, or call it from a command-line window.  If you have PowerShell installed, it offers better features than the default command line.

#### macOS

On macOS, the most recent version of SBCL is available through the Homebrew package manager:

```sh
$ brew install sbcl
```

You will also want to install the `rlwrap` package:

```sh
$ brew install rlwrap
```

Then you can run SBCL from the terminal as follows:

```sh
$ rlwrap sbcl
```

#### Linux

Most Linux distros have the latest, or a very recent, version of SBCL in their official package repositories. You can install it with the package manager for your Linux distro as you would any other software.

You will also want to install the `rlwrap` package, and run SBCL from the shell as follows:

```sh
$ rlwrap sbcl
```

#### Debian/Ubuntu

The packages for SBCL in Apt tend to be severely out-of-date, so if you're using Debian or Ubuntu, you will have a little extra work to do.

First, install the version of SBCL available for your distro in Apt.

Using the old version of SBCL, you may now compile the latest version of SBCL from source.

### Installing Quicklisp

Once you have SBCL up and running, the first thing you should do is install Quicklisp.  It's a package manager for Lisp, and will allow you to dynamically load community-supported libraries (which typically include ASDF systems and their associated Lisp packages) into your running Lisp image.  It also automatically resolves dependencies for your own Lisp projects.

From the command-line, download the Quicklisp installation script and release signing key:

```sh
$ curl -O https://beta.quicklisp.org/quicklisp.lisp
$ curl -O https://beta.quicklisp.org/quicklisp.lisp.asc
```

Verify the installation script against its signing key before running it:


```sh
$ gpg --verify quicklisp.lisp.asc quicklisp.lisp
```

If the verification succeeds, you can launch SBCL and load the installation script at the same time:

```sh
$ rlwrap sbcl --load quicklisp.lisp
```

You will then find yourself at the Lisp REPL, with some instructions:

```lisp
  ==== quicklisp quickstart loaded ====

    To continue, evaluate: (quicklisp-quickstart:install)
```

Like the instructions tell you, type that at the REPL prompt:

```lisp
* (quicklisp-quickstart:install)
```

Once it finishes downloading its dependencies and setting up the working folders, it will prompt you with another message confirming it was installed.  Now you will want to set it up to load automatically every time you start SBCL:

```lisp
* (ql:add-to-init-file)
```

This function will tell you exactly what it's doing before it changes your lisp `.*rc` file.  If you're ready, press Enter to continue when it prompts you to do so.

That's it!  You can now use `ql:system-apropos` to search for libraries, and `ql:quickload` to download and install them.

You can put your Lisp projects under `~/quicklisp/local-projects/` so that Quicklisp can find them automatically; then you will be able to quickload your own projects just like the libraries distributed with Quicklisp.

### Choosing a Text Editor

All you need to get started with Lisp is a simple, no-frills text editor with syntax highlighting, so that you can focus on learning Lisp itself and not get distracted by fiddling with an unfamiliar environment. A few worth checking out are:

* For Windows, macOS, and Linux:
  * VSCode
  * Sublime Text
  * Atom
  * UltraEdit
  * gedit
* macOS only
  * BBEdit
* Windows only
  * Notepad++

Your favourite existing text editor should also be fine, just make sure it has syntax highlighting for Lisp and stays out of your way.
