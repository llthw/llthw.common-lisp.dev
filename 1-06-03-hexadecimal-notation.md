# Exercise 1.6.3

## Hexadecimal Integer Notation

### In the REPL

```lisp
#xFF

#x00

#x0123456789ABCDEF

(parse-integer "FF" :radix 16)

(parse-integer "0123456789ABCDEF" :radix 16)

(format nil "~x" 255)

(format nil "~x" 81985529216486895)

(type-of #xFF)

(+ #xDEADBEEF #xFF)

(format nil "~x" #x0123456789ABCDEF)
```

### What You Should See

There exists a built-in reader macro to allow for hexadecimal literals.

```lisp
* #xFF
255

* #x00
0

* #x0123456789ABCDEF
81985529216486895
```

You also have access to appropriate options in `parse-integer` and `format` to read or write hexadecimal numbers.

```lisp
* (parse-integer "FF" :radix 16)
255
2

* (parse-integer "0123456789ABCDEF" :radix 16)
81985529216486895
16

* (format nil "~x" 255)
"FF"

* (format nil "~x" 81985529216486895)
"123456789ABCDEF"
```

Of course, the reader macro expands into a number...

```lisp
* (type-of #xFF)
(INTEGER 0 4611686018427387903)
```

...which means you can use it anywhere you can use numbers.

```lisp
* (+ #xDEADBEEF #xFF)
3735928814

* (format nil "~x" #x0123456789ABCDEF)
"123456789ABCDEF"
```
