# Exercise 1.6.11

## More Arithmetic

### In the REPL

```lisp
(+ 1 1.0)

(+ 1 #C(0.0 1.0))

(/ 9 2.0)

(floor 9/2)

(floor 9 2)

(ceiling 9/2)

(ceiling 9 2)
```

### What You Should See

```lisp
* (+ 1 1.0)
2.0

* (+ 1 #C(0.0 1.0))
#C(1.0 1.0)

* (/ 9 2.0)
4.5

* (floor 9/2)
4
1/2

* (floor 9 2)
4
1

* (ceiling 9/2)
5
-1/2

* (ceiling 9 2)
5
-1
```

You can perform arithmetic across multiple number types, as Common Lisp takes into consideration the law of propagation of floats; while normally division returns a rational number, as stated in the previous exercise, if you supply a Real number represented as a Float, you will get a float back instead of a rational.

Common Lisp also supports Floor and Ceiling division, which return the quotient and remainder as multiple values---although sometimes that remainder can be deceptive.  These functions also can take a single rational number, rounding down or up to the nearest integer and returning the remainder as a ratio.
