# Exercise 1.4.3

## Dot-Notation

You can see that when we cons two atoms together, we get back a Dotted Pair. This is a `read`able representation of Cons-Cells. That is, you can use it directly, rather than calling `cons`.

```lisp
* (cons 'a 'b)
(A . B)

* '(a . b)
(A . B)
```

These two representations are equivalent.

```lisp
* (equal (cons 'a 'b) '(a . b))
T
```
