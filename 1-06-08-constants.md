# Exercise 1.6.8

## Numeric Constants

### In the REPL

```lisp
pi

(= pi pi)

(eq pi pi)

(eql pi pi)

(equal pi pi)

(equalp pi pi)
```

### What You Should See

The constant `pi` is defined for you.

```lisp
* pi
3.141592653589793d0
```

It's always equal to itself, obviously, but its exact representation depends on the float precision of a particular Lisp implementation.

```lisp
* (= pi pi)
T

* (eq pi pi)
T

* (eql pi pi)
T

* (equal pi pi)
T

* (equalp pi pi)
T
```
