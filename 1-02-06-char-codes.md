# Exercise 1.2.6

## Character Codes

Character objects all have numeric values, called `codes`, corresponding to either the code-points in the Unicode table, or the current encoding of your running Lisp image.  Lisp comes with a lot of useful tools for working with characters and strings as numbers and vectors/arrays, and where the standard falls short, there are several popular libraries you can always rely on.

You can get the code for a character object with the function `char-code`, and the character object for a code with `code-char`, as follows:

```lisp
(code-char #x61)
(char-code #\a)
```
{% hint style='info' %}
See the symbol `#x61` in the example above? That's the hexadecimal (base-16) number `0x61`, which is `97` in decimal (base-10). Sometimes it's more convenient to reference integers using a different notation than decimal. The Unicode tables are listed using hexadecimal, for instance---so you can just type the hex-number using the Sharpsign-X syntax. There will be more discussion on numbers and notation in Chapter 1.6.
{% endhint %}

### What You Should See

```lisp
* (code-char #x61)
#\a
* (char-code #\a)
97
```
